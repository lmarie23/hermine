# SPDX-FileCopyrightText: 2021 Hermine-team <hermine@inno3.fr>
# SPDX-FileCopyrightText: 2022 Martin Delabre <gitlab.com/delabre.martin>
#
# SPDX-License-Identifier: AGPL-3.0-only
import json
import logging
from functools import lru_cache, reduce
from itertools import product
from typing import Iterable, List, Any

from django.db import transaction
from license_expression import get_spdx_licensing, BaseSymbol

from cube.models import License, Obligation, Derogation
from cube.serializers import LicenseSerializer

logger = logging.getLogger(__name__)
licensing = get_spdx_licensing()


@lru_cache(maxsize=1024)
def has_ors(spdx_expression: str):
    parsed = licensing.parse(spdx_expression)

    if parsed is None or isinstance(parsed, BaseSymbol):
        return False

    if "OR" in parsed.operator:
        return True

    for sub_expression in parsed.args:
        if has_ors(sub_expression):
            return True

    return False


@lru_cache(maxsize=1024)
def is_ambiguous(spdx_expression: str):
    """
    Because of unreliable metadata, many "License1 AND License2" expressions
    actually meant to be "License1 OR License2". This function checks weither
    an expressions can be trusted or not.

    :param spdx_expression: an expression to test
    :type spdx_expression: str
    :return: whether expression needs to be confirmed
    :rtype: bool
    """
    parsed = licensing.parse(spdx_expression)
    if parsed is None or isinstance(parsed, BaseSymbol) or has_ors(spdx_expression):
        return False

    return True


def get_ands_corrections(spdx_expression: str) -> Iterable[str]:
    if not is_ambiguous(spdx_expression):
        return {spdx_expression}

    return {
        str(expression)
        for expression in _get_ands_corrections_expressions(
            licensing.parse(spdx_expression)
        )
    }


def _get_ands_corrections_expressions(parsed):
    if isinstance(parsed, BaseSymbol):
        return {parsed}

    simplified = parsed.simplify()
    simplified_or_expression = reduce(lambda a, b: a | b, simplified.args).simplify()

    if all(isinstance(arg, BaseSymbol) for arg in parsed.args):
        # no sub expressions
        return {simplified, simplified_or_expression}

    combinations = list(
        product(*(_get_ands_corrections_expressions(arg) for arg in parsed.args))
    )
    and_combinations = [
        reduce(lambda a, b: a & b, combination).simplify()
        for combination in combinations
    ]
    or_combinations = [
        reduce(lambda a, b: a | b, combination).simplify()
        for combination in combinations
    ]

    return {
        str(simplified),
        simplified_or_expression,
        *and_combinations,
        *or_combinations,
    }


def check_licenses_against_policy(release):
    response = {}
    usages_lic_never_allowed = set()
    usages_lic_context_allowed = set()
    usages_lic_unknown = set()
    involved_lic = set()
    derogations = set()

    usages = release.usage_set.all()

    for usage in usages:
        for license in usage.licenses_chosen.all():
            involved_lic.add(license)

            if license.allowed == License.ALLOWED_ALWAYS:
                continue

            license_derogations = Derogation.objects.for_usage(usage).filter(
                license=license
            )
            if license_derogations.exists():
                for derogation in license_derogations:
                    derogations.add(derogation)
                continue

            if license.allowed == License.ALLOWED_NEVER:
                usages_lic_never_allowed.add(usage)
            elif license.allowed == License.ALLOWED_CONTEXT:
                usages_lic_context_allowed.add(usage)
            elif not license.allowed:
                usages_lic_unknown.add(usage)

    response["usages_lic_never_allowed"] = usages_lic_never_allowed
    response["usages_lic_context_allowed"] = usages_lic_context_allowed
    response["usages_lic_unknown"] = usages_lic_unknown
    response["involved_lic"] = involved_lic
    response["derogations"] = derogations

    return response


@lru_cache(maxsize=1024)
def explode_spdx_to_units(spdx_expr: str) -> List[str]:
    """Extract a list of every license from a SPDX valid expression.

    :param spdx_expr: A string that represents a valid SPDX expression. (Like ")
    :type spdx_expr: string
    :return: A list of valid SPDX licenses contained in the expression.
    :rtype: list
    """
    licensing = get_spdx_licensing()
    parsed = licensing.parse(spdx_expr)
    if parsed is None:
        return []
    return sorted(list(parsed.objects))


def create_or_update_license(license_dict):
    create = False
    try:
        license_instance = License.objects.get(spdx_id=license_dict["spdx_id"])
    except License.DoesNotExist:
        license_instance = License(spdx_id=license_dict["spdx_id"])
        license_instance.save()
        create = True
    s = LicenseSerializer(license_instance, data=license_dict)
    s.is_valid(raise_exception=True)
    s.save()
    return create


def get_license_triggered_obligations(
    license: License, exploitation: str = None, modification: str = None
):
    """
    Get triggered obligations for a license and a usage context
    (if the component has been modified and how it's being distributed)

    :param license: A License instance
    :param exploitation: A value from Usage.EXPLOITATION_CHOICES
    :param modification: A value from Usage.MODIFICATION_CHOICES
    :return: A queryset or list of Obligation instances
    """
    obligations = license.obligation_set.all()

    if modification is not None:
        obligations = obligations.filter(trigger_mdf__contains=modification)

    if exploitation is not None:
        obligations = [
            o
            for o in obligations
            if (
                (o.trigger_expl in exploitation)
                or (exploitation in o.trigger_expl)
                or (o.passivity == "Passive")
            )  # Poor man bitwise OR
        ]

    return obligations


def get_licenses_triggered_obligations(
    licenses: Iterable[License], exploitation: str = None, modification: str = None
):
    obligations_pk = set()
    for license in licenses:
        obligations_pk.update(
            o.pk
            for o in get_license_triggered_obligations(
                license, exploitation, modification
            )
        )
    return Obligation.objects.filter(pk__in=obligations_pk)


def get_usages_obligations(usages):
    """
    Get triggered obligations for a list of usages.

    :param usages: A iterable of Usage objects
    :return: A tuple with a list of generics, a list of licenses which
    obligations have no generics and the sorted list of licenses involved
    """
    generics_involved = set()
    orphaned_licenses = set()
    licenses_involved_set = set()

    for usage in usages:
        for license in usage.licenses_chosen.all():
            licenses_involved_set.add(license)
            for obligation in get_license_triggered_obligations(
                license, usage.exploitation, usage.component_modified
            ):
                if obligation.generic:
                    generics_involved.add(obligation.generic)
                else:
                    orphaned_licenses.add(license)
    licenses_involved = sorted(licenses_involved_set, key=lambda x: x.spdx_id)
    return generics_involved, orphaned_licenses, licenses_involved


def export_licenses(indent=False):
    serializer = LicenseSerializer(License.objects.all(), many=True)
    data = json.dumps(serializer.data, indent=4 if indent else None)
    return data


@transaction.atomic()
def handle_licenses_json(data):
    licenseArray = json.loads(data)
    # Handling case of a JSON that only contains one license and is not a list
    # (single license purpose)
    if type(licenseArray) is dict:
        create_or_update_license(licenseArray)
    # Handling case of a JSON that contains multiple licenses and is a list
    # (multiple licenses purpose)
    elif type(licenseArray) is list:
        created, updated = 0, 0
        for license in licenseArray:
            if create_or_update_license(license):
                created += 1
            else:
                updated += 1

        logger.info(f"Licenses : {created} created / {updated} updated")
    else:
        logger.info("Type of JSON neither is a list nor a dict")
