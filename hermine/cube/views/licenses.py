# SPDX-FileCopyrightText: 2021 Hermine-team <hermine@inno3.fr>
# SPDX-FileCopyrightText: 2022 Martin Delabre <gitlab.com/delabre.martin>
#
# SPDX-License-Identifier: AGPL-3.0-only
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.http import HttpResponse
from django.urls import reverse_lazy, reverse
from django.views.generic import (
    ListView,
    DetailView,
    FormView,
    UpdateView,
    CreateView,
    DeleteView,
)
from odf.opendocument import OpenDocumentText
from odf.style import Style, TextProperties, ParagraphProperties
from odf.text import H, P, Span

from cube.forms.importers import ImportLicensesForm, ImportGenericsForm
from cube.models import License, Generic, Obligation
from cube.views.mixins import SearchMixin, LicenseRelatedMixin


class FormErrorsToMessagesMixin:
    def form_invalid(self, form):
        for field, errs in form.errors.items():
            for err in errs:
                messages.add_message(self.request, messages.ERROR, err)
        return super().form_invalid(form)

    def form_valid(self, form):
        try:
            form.save()
        except ValidationError as e:
            messages.add_message(self.request, messages.ERROR, e.message)
            return super().form_invalid(form)
        messages.add_message(self.request, messages.SUCCESS, "File imported.")
        return super().form_valid(form)


class LicensesListView(
    LoginRequiredMixin, SearchMixin, FormErrorsToMessagesMixin, ListView, FormView
):
    model = License
    context_object_name = "licenses"
    paginate_by = 50
    form_class = ImportLicensesForm
    success_url = reverse_lazy("cube:licenses")
    search_fields = ("long_name", "spdx_id")

    def post(self, *args, **kwargs):
        self.object_list = self.get_queryset()
        return super().post(*args, **kwargs)


class LicenseDetailView(LoginRequiredMixin, DetailView):
    model = License


class LicenseUpdateView(LoginRequiredMixin, UpdateView):
    model = License
    fields = [
        "spdx_id",
        "long_name",
        "url",
        "copyleft",
        "law_choice",
        "venue_choice",
        "status",
        "allowed",
        "allowed_explanation",
        "patent_grant",
        "foss",
        "non_commercial",
        "ethical_clause",
        "warranty",
        "liability",
        "comment",
        "verbatim",
    ]


class PrintLicense(LoginRequiredMixin, DetailView):
    model = License

    def render_to_response(self, context, **response_kwargs):
        filename = self.object.spdx_id + ".odt"
        response = HttpResponse(content_type="application/vnd.oasis.opendocument.text")
        response["Content-Disposition"] = "attachment; filename=%s" % filename

        textdoc = OpenDocumentText()
        s = textdoc.styles

        h1style = Style(name="Heading 1", family="paragraph")
        h1style.addElement(
            TextProperties(attributes={"fontsize": "24pt", "fontweight": "bold"})
        )
        s.addElement(h1style)
        h1style.addElement(ParagraphProperties(attributes={"marginbottom": "1cm"}))
        h2style = Style(name="Heading 2", family="paragraph")
        h2style.addElement(
            TextProperties(attributes={"fontsize": "18pt", "fontweight": "bold"})
        )
        h2style.addElement(
            ParagraphProperties(
                attributes={"marginbottom": "0.6cm", "margintop": "0.4cm"}
            )
        )
        s.addElement(h2style)

        h3style = Style(name="Heading 3", family="paragraph")
        h3style.addElement(
            TextProperties(attributes={"fontsize": "14pt", "fontweight": "bold"})
        )
        h3style.addElement(
            ParagraphProperties(
                attributes={"marginbottom": "0.2cm", "margintop": "0.4cm"}
            )
        )

        s.addElement(h3style)

        itstyle = Style(name="Italic", family="paragraph")
        itstyle.addElement(TextProperties(attributes={"textemphasize": "true"}))
        itstyle.addElement(ParagraphProperties(attributes={"margintop": "3cm"}))

        s.addElement(itstyle)

        # An automatic style
        boldstyle = Style(name="Bold", family="text")
        boldprop = TextProperties(fontweight="bold")
        boldstyle.addElement(boldprop)
        textdoc.automaticstyles.addElement(boldstyle)

        textdoc.automaticstyles.addElement(itstyle)

        # Text
        h = H(outlinelevel=1, stylename=h1style, text=self.object.long_name)
        textdoc.text.addElement(h)
        h = H(outlinelevel=1, stylename=h2style, text=self.object.spdx_id)
        textdoc.text.addElement(h)

        p = P(text="Validation Color: ")
        v = Span(stylename=boldstyle, text=self.object.allowed)
        p.addElement(v)
        textdoc.text.addElement(p)

        if self.object.allowed_explanation is not None:
            p = P(text="Explanation: ")
            v = Span(stylename=boldstyle, text=self.object.allowed_explanation)
            p.addElement(v)
            textdoc.text.addElement(p)

        p = P(text="Copyleft: ")
        v = Span(stylename=boldstyle, text=self.object.copyleft)
        p.addElement(v)
        textdoc.text.addElement(p)

        p = P(text="Considered as Free Open Source Sofware: ")
        v = Span(stylename=boldstyle, text=self.object.foss)
        p.addElement(v)
        textdoc.text.addElement(p)

        p = P(text="Approved by OSI: ")
        v = Span(stylename=boldstyle, text=self.object.osi_approved)
        p.addElement(v)
        textdoc.text.addElement(p)

        p = P(text="Has an ethical clause: ")
        v = Span(stylename=boldstyle, text=self.object.ethical_clause)
        p.addElement(v)
        textdoc.text.addElement(p)

        if self.object.verbatim:
            p = P(text="Verbatim: ")
            value = Span(text=self.object.verbatim)
            p.addElement(value)
            textdoc.text.addElement(p)

        if self.object.comment:
            p = P(text="Comment: ")
            value = Span(stylename=boldstyle, text=self.object.comment)
            p.addElement(v)
            textdoc.text.addElement(p)

        h = H(outlinelevel=1, stylename=h2style, text="List of identified obligations")
        textdoc.text.addElement(h)

        for o in self.object.obligation_set.all():
            h = H(outlinelevel=1, stylename=h3style, text=o.name)
            textdoc.text.addElement(h)
            generic = o.generic

            if generic:
                p = P(text="Related Generic Obligation: ")
                v = Span(stylename=boldstyle, text=generic)
                p.addElement(v)
                textdoc.text.addElement(p)

            p = P(text="Passivity: ")
            v = Span(stylename=boldstyle, text=o.passivity)
            p.addElement(v)
            textdoc.text.addElement(p)

            p = P(text="Mode of exploitation that triggers this obligation: ")
            v = Span(stylename=boldstyle, text=o.trigger_expl)
            p.addElement(v)
            textdoc.text.addElement(p)

            p = P(text="Status of modification that triggers this obligation: ")
            v = Span(stylename=boldstyle, text=o.trigger_mdf)
            p.addElement(v)
            textdoc.text.addElement(p)

            if o.verbatim:
                p = P(text="Verbatim of the obligation: ")
                v = Span(text=o.verbatim)
                p.addElement(v)
                textdoc.text.addElement(p)

        p = P(
            stylename=itstyle,
            text="This license interpretation was exported from a Hermine project."
            + " https://hermine-foss.org/.",
        )
        textdoc.text.addElement(p)

        textdoc.save(response)

        return response


class ObligationCreateView(LoginRequiredMixin, LicenseRelatedMixin, CreateView):
    model = Obligation
    fields = ("generic", "name", "verbatim", "passivity", "trigger_expl", "trigger_mdf")
    license = None

    def get_success_url(self):
        return reverse("cube:license", args=[self.object.license.id])


class ObligationUpdateView(LoginRequiredMixin, UpdateView):
    model = Obligation
    fields = ("generic", "name", "verbatim", "passivity", "trigger_expl", "trigger_mdf")

    def get_success_url(self):
        return reverse("cube:license", args=[self.object.license.id])


class ObligationDeleteView(LoginRequiredMixin, DeleteView):
    model = Obligation

    def get_success_url(self):
        return reverse("cube:license", args=[self.object.license.id])


class GenericListView(
    LoginRequiredMixin, FormErrorsToMessagesMixin, ListView, FormView
):
    model = Generic
    context_object_name = "generics"
    form_class = ImportGenericsForm
    success_url = reverse_lazy("cube:generics")

    def post(self, *args, **kwargs):
        self.object_list = self.get_queryset()
        return super().post(*args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = {
            "generics_incore": self.object_list.filter(in_core=True),
            "generics_outcore": self.object_list.filter(in_core=False),
        }
        context.update(**kwargs)
        return super().get_context_data(object_list=object_list, **context)


class GenericDetailView(LoginRequiredMixin, DetailView):
    model = Generic
    context_object_name = "generic"
    template_name = "cube/generic.html"
