<!---  
SPDX-FileCopyrightText: 2022 Hermine team <hermine@inno3.fr> 
SPDX-License-Identifier: CC-BY-4.0
-->

# The Data Model



## Models for license management

```{eval-rst}
.. automodule:: cube.models.licenses
    :members: 
```



## Models for validation rules

```{eval-rst}
.. automodule:: cube.models.policy
    :members: 
```



## Models for internal product management

```{eval-rst}
.. automodule:: cube.models.products
    :members: 
```

## Models for 3rd party components

```{eval-rst}
.. automodule:: cube.models.components
    :members: 
```
